<?php
use Illuminate\Http\Request;
use \App\Article;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::get('/', 'WelcomeController@index');

Route::get('/home', 'HomeController@index');

Route::controllers([
    'auth'     => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::get('/articles', function () {
    // If the Content-Type and Accept headers are set to 'application/json',
    // this will return a JSON structure. This will be cleaned up later.
    return Article::all();
});

Route::get('articles/{id}', function ($id) {
    return Article::find($id);
});

Route::post('/articles', function (Request $request) {
    Article::create($request->all());

    return 200;
});

Route::put('articles/{id}', function (Request $request, $id) {
    $article = Article::findOrFail($id);
    $article->update($request->all());

    return $article;
});

Route::delete('articles/{id}', function ($id) {
    Article::find($id)->delete();

    return 204;
});

//API routes

Route::post('oauth/access_token', function () {
    return Response::json(Authorizer::issueAccessToken());
});

Route::get('/api', ['before' => 'oauth', function () {
    // return the protected resource
    //echo “success authentication”;
    $user_id = Authorizer::getResourceOwnerId(); // the token user_id
    $user    = \App\User::find($user_id); // get the user data from database
    return Response::json($user);
}]);

Route::group(['prefix' => 'api', 'before' => 'oauth'], function () {
    Route::get('/articles', function () {
        // If the Content-Type and Accept headers are set to 'application/json',
        // this will return a JSON structure. This will be cleaned up later.
        return Article::all();
    });
    Route::get('/articles/{article}', 'ArticleController@show');
    Route::post('/articles', 'ArticleController@store');
    Route::put('/articles/{id}', 'ArticleController@update');
    Route::delete('/articles/{id}', 'ArticleController@delete');

});
