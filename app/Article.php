<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model {

	//add columns of article table so that we can use here by referring article model.
	protected $fillable = ['title', 'body','user_id'];

}
